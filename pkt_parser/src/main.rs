extern crate core;

use std::sync::mpsc::channel;
use std::thread;
use std::thread::sleep;
use std::time::Duration;
use pcap::{Capture, Device, Error, Packet};

mod pkt_parser {
    use std::fmt;
    use std::fmt::{Display, Formatter};

    // Utility modules
    mod utils {
        use std::fmt;

        struct HexSlice<'a>(&'a [u8]);

        impl<'a> HexSlice<'a> {
            fn new<T>(data: &'a T) -> HexSlice<'a>
                where
                    T: ?Sized + AsRef<[u8]> + 'a,
            {
                HexSlice(data.as_ref())
            }
        }

        // You can choose to implement multiple traits, like Lower and UpperHex
        impl fmt::Display for HexSlice<'_> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                for byte in self.0 {
                    // Decide if you want to pad the value or have spaces inbetween, etc.
                    write!(f, "{:x} ", byte)?;
                }
                Ok(())
            }
        }

        trait HexDisplayExt {
            fn hex_display(&self) -> HexSlice<'_>;
        }

        impl<T> HexDisplayExt for T
            where
                T: ?Sized + AsRef<[u8]>,
        {
            fn hex_display(&self) -> HexSlice<'_> {
                HexSlice::new(self)
            }
        }

        pub fn mac_address_to_string(address: &[u8]) -> String {
            address.hex_display().to_string().replace(" ", "")
        }

        pub fn ipv4_address_to_string(address: &[u8]) -> String {
            address.iter().map(|b| b.to_string()).collect::<Vec<String>>().join(".")
        }

    }

    #[derive(Debug, Clone)]
    pub struct DecodeError{msg: String}

    impl Display for DecodeError {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            write!(f, "Decode error: {}", self.msg)
        }
    }

    #[derive(Debug, Clone)]
    enum EtherType {
        Ipv4,
        Ipv6,
        ARP,
    }


    #[derive(Debug, Clone)]
    pub struct EthernetHeader {
        dest: String,
        src: String,
        ether_type: EtherType,
    }

    impl EthernetHeader {
        pub fn decode(data: Vec<u8>) -> (Result<Self, DecodeError>, Vec<u8>) {
            let len = data.len();
            // Extracting data
            let eth_header = &data[0..14];
            let ether_type_vec = &eth_header[12..14];
            let ether_type = ((ether_type_vec[0] as u16) << 8) | ether_type_vec[1] as u16;
            // println!("Entire header: {:x?} \n Destination MAC address: {:x?} Source MAC address: {:x?} Ether type: {:x?}", eth_header, &eth_header[0..6], &eth_header[6..12], ether_type);
            let ether_payload = &data[14..len];

            // EthernetHeader creation

            let real_ether_type = match ether_type {
                0x0800 => EtherType::Ipv4,
                0x0806 => EtherType::ARP,
                0x86DD => EtherType::Ipv6,
                val => return (
                    Err(DecodeError{msg: format!("Cannot get the correct ether type, received 0x{:x}", val).to_string()}),
                    data
                )
            };
            (
                Ok(EthernetHeader{dest: utils::mac_address_to_string(&eth_header[0..6]), src: utils::mac_address_to_string(&eth_header[6..12]) , ether_type: real_ether_type}),
                Vec::from(ether_payload)
            )
        }
    }

    #[derive(Debug, Clone)]
    pub enum Protocol {
        TCP,
        UDP
    }

    #[derive(Debug, Clone)]
    pub struct Ipv4Header {
        dest: String,
        src: String,
        protocol: Protocol,
    }

    impl Ipv4Header {
        pub fn decode(data: Vec<u8>) -> (Result<Self, DecodeError>, Vec<u8>) {
            let len = data.len();
            let header_len = (data[0] & 0x0f ) as usize;
            // println!("IPV4 header has len {} byte", header_len * 4);
            let protocol = match &data[9] {
                0x06 => Protocol::TCP,
                0x11 => Protocol::UDP,
                value => return (
                    Err(DecodeError{ msg: format!("Unable to identify level 4 protocol. Received 0x{:x}", value) }),
                    data
                )
            };

            let src_address = utils::ipv4_address_to_string(&data[12..16]);
            let dest_address = utils::ipv4_address_to_string(&data[16..20]);
            (
                Ok(Ipv4Header{src: src_address, dest: dest_address, protocol}),
                Vec::from(&data[header_len..len])
            )
        }

        pub fn get_protocol(&self) -> Protocol {
            self.protocol.clone()
        }
    }

    #[derive(Debug, Clone)]
    pub struct UDPHeader {
        dest: u16,
        src: u16,
    }

    impl UDPHeader {
        pub fn decode(data: Vec<u8>) -> (Result<Self, DecodeError>, Vec<u8>) {
            let dest = ((data[0] as u16) << 8) | data[1] as u16;
            let src = ((data[2] as u16) << 8) | data[3] as u16;
            (
                Ok(UDPHeader{dest, src}),
                Vec::from(&data[8..])
            )
        }
    }

    #[derive(Debug, Clone)]
    pub struct TCPHeader {
        dest: u16,
        src: u16,
    }

    impl TCPHeader {
        pub fn decode(data: Vec<u8>) -> (Result<Self, DecodeError>, Vec<u8>) {
            let dest = ((data[0] as u16) << 8) | data[1] as u16;
            let src = ((data[2] as u16) << 8) | data[3] as u16;
            (
                Ok(TCPHeader{dest, src}),
                Vec::from(&data[20..])
            )
        }
    }
}

fn decode_packet(packet: Vec<u8>) -> Result<(), DecodeError>{
    let (eth_header_result, eth_payload) = EthernetHeader::decode(packet);
    let eth_header = eth_header_result?;

    println!("{:?}", eth_header);

    let (ipv4_header_result, ipv4_payload) = Ipv4Header::decode(eth_payload);
    let ipv4_header = ipv4_header_result?;

    println!("{:?}", ipv4_header);
    match ipv4_header.get_protocol() {
        Protocol::UDP => {
            let (udp_header_result, udp_payload) = UDPHeader::decode(ipv4_payload);
            let udp_header = udp_header_result?;
            println!("{:?}", udp_header);
        }
        Protocol::TCP => {
            let (tcp_header_result, tcp_payload) = TCPHeader::decode(ipv4_payload);
            let tcp_header = tcp_header_result?;
            println!("{:?}", tcp_header);
        }
    }
    Ok(())
}

use crate::pkt_parser::{DecodeError, Ipv4Header, EthernetHeader, Protocol, UDPHeader, TCPHeader};

fn main() {
    println!("Packet parser - v0.0.1");
    println!("The aim of this library is to parse raw data received from pcap");

    println!("Multithread example");
    let (tx, rx) = channel();

    let t2 = thread::spawn(move || {
        let tx = tx.clone();
        let devices = Device::list().unwrap();
        let mut i:i32 = 0;
        let mut device_index = -1;

        for device in &devices {
            if device.name.contains("5CCAC7E7-C666-4548-8F12-005633BAEE62") {
                device_index = i;
            }
            i += 1;
        }
        let main_device = match device_index {
            -1 => panic!("Not found!"),
            v => devices[v as usize].clone()
        };

        println!("Using {}", main_device.name);

        let mut cap = main_device.open().unwrap();

        while let Ok(packet) = cap.next() {
            tx.send(Vec::from(packet.data)).unwrap();
        }

        drop(tx);
    });


    while let Ok(packet) = rx.recv() {
        match decode_packet(packet){
            Ok(()) => (),
            Err(e) => println!("{}", e)
        };
    }
}
