use pcap::{Device, Capture, Packet};

fn print_device(device: &Device){
    println!("{:?}", device);
}

fn main() {
    let devices = Device::list().unwrap();
    devices.iter().for_each(|d| print_device(&d));
    let mut i:i32 = 0;
    let mut device_index = -1;
    for device in &devices {
        if device.name.contains("5CCAC7E7-C666-4548-8F12-005633BAEE62") {
            device_index = i;
        }
        i += 1;
    }
    let main_device = match device_index {
        -1 => panic!("Not found!"),
        v => devices[v as usize].clone()
    };

    println!("Using {}", main_device.name);

    let mut cap = main_device.clone().open().unwrap();
    (1..3).for_each(|i| println!("#{} {:?} {:?}",i, main_device.addresses[0], cap.next().unwrap()));

    // We need a way to extract information from the data in the Packet.
}