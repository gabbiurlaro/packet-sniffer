use pcap::{Capture, Error, Packet};
use std::{fmt, thread, vec};
use std::fmt::{Display, Formatter, write};
use std::sync::mpsc::channel;

// Header is a marker trait, that show if a
// struct can be an internet header
trait Header{}

// HexSlice is an utility to print
// mac addresses as hex.
struct HexSlice<'a>(&'a [u8]);

impl<'a> HexSlice<'a> {
    fn new<T>(data: &'a T) -> HexSlice<'a>
        where
            T: ?Sized + AsRef<[u8]> + 'a,
    {
        HexSlice(data.as_ref())
    }
}

// You can choose to implement multiple traits, like Lower and UpperHex
impl fmt::Display for HexSlice<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for byte in self.0 {
            // Decide if you want to pad the value or have spaces inbetween, etc.
            write!(f, "{:x} ", byte)?;
        }
        Ok(())
    }
}

trait HexDisplayExt {
    fn hex_display(&self) -> HexSlice<'_>;
}

impl<T> HexDisplayExt for T
    where
        T: ?Sized + AsRef<[u8]>,
{
    fn hex_display(&self) -> HexSlice<'_> {
        HexSlice::new(self)
    }
}

// DecodeError
//
//
#[derive(Debug, Clone)]
struct DecodeError{msg: String}

impl Display for DecodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Decode error: {}", self.msg)
    }
}

#[derive(Debug, Clone)]
enum EtherType {
    Ipv4,
    Ipv6,
    ARP,
}


#[derive(Debug, Clone)]
struct EthernetHeader {
    dest: String,
    src: String,
    ether_type: EtherType,
}

impl EthernetHeader {
    pub fn decode(data: Vec<u8>) -> (Result<Self, DecodeError>, Vec<u8>) {
        let len = data.len();
        let eth_header = &data[0..14];
        let ether_type_vec = &eth_header[12..14];
        let ether_type = ((ether_type_vec[0] as u16) << 8) | ether_type_vec[1] as u16;
        // println!("Entire header: {:x?} \n Destination MAC address: {:x?} Source MAC address: {:x?} Ether type: {:x?}", eth_header, &eth_header[0..6], &eth_header[6..12], ether_type);
        let ether_payload = &data[14..len];
        let real_ether_type = match ether_type {
            0x0800 => Ok(EtherType::Ipv4),
            0x0806 => Ok(EtherType::ARP),
            0x86DD => Ok(EtherType::Ipv6),
            _ => Err("Ether type not recognized")
        };
        (
            Ok(EthernetHeader{dest:mac_address_to_string(&eth_header[0..6]), src:mac_address_to_string(&eth_header[6..12]) , ether_type: real_ether_type.unwrap()}),
            Vec::from(ether_payload)
        )
    }
}

#[derive(Debug, Clone)]
enum Protocol {
    UDP,
    TCP,
    Undefined
}

#[derive(Debug, Clone)]
struct Ipv4Header {
    dest: String,
    src: String,
    protocol: Protocol,
}
impl Ipv4Header {
    pub fn decode(data: &[u8], len: usize) -> (Result<Self, DecodeError>, &[u8]) {
        let header_len = (data[0] & 0x0f ) as usize;
        // println!("IPV4 header has len {} byte", header_len * 4);
        let protocol = match &data[9] {
            0x06 => Protocol::TCP,
            0x11 => Protocol::UDP,
            _ => return (
                Err(DecodeError{ msg: "Unable to idenitify level 4 protocol".to_string() }),
                data
            )
        };

        let src_address = ipv4_address_to_string(&data[12..16]);
        let dest_address = ipv4_address_to_string(&data[16..20]);
        (
            Ok(Ipv4Header{src: src_address, dest: dest_address, protocol}),
            &data[header_len..len]
        )
    }
}

#[derive(Debug, Clone)]
struct UDPHeader {
    src_port: u32,
    dest_port: u32,
}

impl UDPHeader {
    pub fn decode(data: &[u8], len: usize) -> (Result<Self, DecodeError>, &[u8]) {
        (
            Ok(UDPHeader{src_port: 1, dest_port: 2}),
            data
        )
    }
}

fn ipv4_address_to_string(address: &[u8]) -> String {
    address.iter().map(|b| b.to_string()).collect::<Vec<String>>().join(".")
}

fn mac_address_to_string(address: &[u8]) -> String {
    address.hex_display().to_string().replace(" ", "")
}

impl Header for EthernetHeader{}
impl Header for Ipv4Header{}
impl Header for UDPHeader{}

fn decode_packet(data: Vec<u8>) {
    /* The decode packet function is a function that demonstrate how is possible to decode
     * packet information from a raw capture. Can be organized in different ways.
     */
    let eth_tuple = EthernetHeader::decode(data);
    let eth_header = eth_tuple.0.unwrap();
    /*
    let ipv4_tuple = Ipv4Header::decode(eth_tuple.1, 120 - 14);
    let ipv4_header = match ipv4_tuple.0 {
        Ok(header) => header,
        Err(e) => { println!("--- \n{}\n---", e); return }
    };
    let udp_tuple = UDPHeader::decode(ipv4_tuple.1, 13);
    let udp_header = udp_tuple.0.unwrap();
    */
    println!("{:?}", eth_header);
    // println!("{:?}", ipv4_header);
    // println!("{:?}", udp_header);
    println!("----------------");
}

fn main() {
    println!("Ex - 02 reading from a pcap file");
    println!("Multithread example");

    let (tx, rx) = channel();
    let t1 = thread::spawn(move || {
        let tx = tx.clone();
        let mut cap = Capture::from_file("sample_capture.pcap").unwrap();
        while let Ok(packet) = cap.next() {
            tx.send(Vec::from(packet.data)).unwrap();
            //println!("{:?}", Vec::from(packet.data));
        }

        drop(tx);
    });

    while let Ok(packet) = rx.recv() {
        decode_packet(packet);
    }
}